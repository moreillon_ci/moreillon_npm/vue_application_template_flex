import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    nav: { label: 'About', icon: 'HomeIcon', index: 1},
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
    nav: { label: 'LOOOOONG', icon: 'HomeIcon', index: 0 },
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
